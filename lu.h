//
// Created by Anandh Swaminathan on 10/16/22.
//
#include "matrix.h"

#ifndef MATRIX_LU_H
#define MATRIX_LU_H
template<unsigned int N>
class LU
{
private:
    Matrix<N, N> L;
    Matrix<N, N> U;
    bool factored = false;
public:
    const Matrix<N, N>& getL() const;

    const Matrix<N, N>& getU() const;

    bool factorize(Matrix<N, N> &A);

    void solve(const Matrix<N, 1>& b, Matrix<N, 1> &x) const;
};

#include "lu.cpp"

#endif //MATRIX_LU_H
