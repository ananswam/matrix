//
// Created by Anandh Swaminathan on 10/16/22.
//
#include "matrix.h"
#include "lu.h"

template<unsigned int N>
const Matrix<N, N>& LU<N>::getL() const {
    assert(factored);
    return L;
}

template<unsigned int N>
const Matrix<N, N>& LU<N>::getU() const {
    assert(factored);
    return U;
}

template<unsigned int N>
bool LU<N>::factorize(Matrix<N, N> &A) {
    L.zeroOut();
    U.zeroOut();
    for (int i = 0; i < N; i++) {

        // U
        for (int k = i; k < N; k++) {
            double sum = 0.0;
            for (int j = 0; j < i; j++) {
                sum += (L.get(i, j) * U.get(j, k));
            }

            U.set(i, k, A.get(i, k) - sum);
        }

        // L
        for (int k = i; k < N; k++) {
            if (i == k) {
                L.set(i, i, 1.0);
            }
            else {
                double sum = 0.0;
                for(int j = 0; j < i; j++) {
                    sum += (L.get(k, j) * U.get(j, i));
                }

                if (U.get(i, i) == 0) {
                    return false;
                }
                double entry = (A.get(k, i) - sum) / U.get(i, i);
                L.set(k, i, entry);
            }
        }
    }
    factored = true;
    return true;
}

template<unsigned int N>
void LU<N>::solve(const Matrix<N, 1>& b, Matrix<N, 1> &x) const {
    assert(factored);
    x.zeroOut();
    Matrix<N, 1> dest1(true);

    // LUx = b
    // Ux = L^-1 b
    // dest1 = L^-1 b
    // then solve Ux = dest1 for x.

    // solve L dest1 = b
    for (int row = 0; row < N; row++) {
        double ans = b.get(row, 0);
        for(int i = 0; i < row; i++) {
            ans -= (L.get(row, i) * dest1.get(i, 0));
        }
        ans /= L.get(row, row);
        dest1.set(row, 0, ans);
    }

    // solve for Ux = dest1

    for (int row = N-1; row >= 0; row--) {
        double ans = dest1.get(row, 0);
        for(int i = row + 1; i < N; i++) {
            ans -= (U.get(row, i) * x.get(i, 0));
        }
        ans /= U.get(row, row);
        x.set(row, 0, ans);
    }
}

