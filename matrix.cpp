//
// Created by Anandh Swaminathan on 10/16/22.
//
#include <iostream>
#include "matrix.h"
#include "lu.h"

template<unsigned int M, unsigned int N>
Matrix<M, N>::Matrix(bool zero) {
    if(zero) {
        for(unsigned i = 0; i < size; i++) {
            data[i] = 0.0;
        }
    }
}

template<unsigned int M, unsigned int N>
Matrix<M, N>::Matrix(const Matrix<M, N>& from) {
    memcpy(data, from.data, sizeof(double[M *N]));
}

template<unsigned int M, unsigned int N>
void Matrix<M, N>::set(int row, int col, double value) {
    data[row * N + col] = value;
}

template<unsigned int M, unsigned int N>
double Matrix<M, N>::get(int row, int col) const {
    return data[row * N + col];
}

template <unsigned int M, unsigned int N>
template <unsigned int P>
void Matrix<M, N>::multiply(const Matrix<N, P> &that, Matrix<M, P> &dest) const {
    for (int i = 0; i < M ; i++) {
        for(int k = 0; k < P; k++) {
            double ans = 0.0;
            for(int j = 0; j < N; j++) {
                ans += (get(i, j) * that.get(j, k));
            }
            dest.set(i, k, ans);
        }
    }
}

template <unsigned int M, unsigned int N>
void Matrix<M, N>::fill(double value) {
    for(int i = 0; i < M; i++) {
        for (int j = 0; j < N; j++) {
            set(i, j, value);
        }
    }
}

template <unsigned int M, unsigned int N>
void Matrix<M, N>::zeroOut() {
    memset(data, 0, sizeof(double[M * N]));
}

template <unsigned int M, unsigned int N>
void Matrix<M, N>::setIdentity() {
    static_assert(M == N, "only square matrices can be identity");
    this->fill(0);
    for(int i = 0; i < M; i++) {
        set(i, i, 1);
    }
}

template <unsigned int M, unsigned int N>
void Matrix<M, N>::add(const Matrix<M, N> &that, Matrix<M, N> &dest) {
    for(int i = 0; i < M; i++) {
        for(int j=0; j < N; j++) {
            dest.set(i, j, get(i, j) + that.get(i, j));
        }
    }
}

template <unsigned int M, unsigned int N>
void Matrix<M, N>::add(const double that, Matrix<M, N> &dest) {
    for(int i = 0; i < M; i++) {
        for(int j=0; j < N; j++) {
            dest.set(i, j, get(i, j) + that);
        }
    }
}

template <unsigned int M, unsigned int N>
void Matrix<M, N>::clamp(double tolerance) {
    for(int i = 0; i < size; i++) {
        if(data[i] < tolerance && data[i] > -tolerance) {
            data[i] = 0.0;
        }
    }
}

template<unsigned int M, unsigned int N>
bool Matrix<M, N>::inv(Matrix<M, N> &dest) {
    static_assert(M == N, "only take inverse of a square matrix");
    LU<N> lu;
    if(!lu.factorize(*this)) {
        return false;
    }

    dest.zeroOut();

    Matrix<N, 1> x;
    Matrix<N, 1> b;

    for(int j = 0; j < N ; j++) {
        b.zeroOut();
        b.set(j, 0, 1.0);
        lu.solve(b, x);
        for(int i = 0; i < N; i++) {
            dest.set(i, j, x.get(i, 0));
        }
    }

    return true;
}

template<>
bool Matrix<1, 1>::inv(Matrix<1, 1> &dest) {
    if(get(0, 0) == 0) {
        return false;
    }
    dest.set(0, 0, 1.0 / get(0, 0));
    return true;
}

template<>
bool Matrix<2, 2>::inv(Matrix<2, 2> &dest) {
    double det = get(0, 0) * get(1, 1) - get(1, 0) * get(0, 1);
    if (det == 0) {
        return false;
    }
    double q = 1 / det;

    dest.set(0, 0, get(1, 1) * q);
    dest.set(1, 1, get(0, 0) * q);

    dest.set(0, 1, get(0, 1) * -q);
    dest.set(1, 0, get(1, 0) * -q);
    return true;
}


template<unsigned int M, unsigned int N>
std::ostream &operator<<(std::ostream &os, Matrix<M, N> const &m) {
    for (int i = 0 ; i < M ; i++) {
        for (int j = 0; j < N; j++) {
            os << m.get(i, j);
            os << (j == N-1 ? '\n': ' ');
        }
    }
    return os;
}
