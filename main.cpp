#include <iostream>
#include <vector>
#include "matrix.h"
#include "lu.h"

using namespace std;


void test_solve_3();
void test_solve_8();
Matrix<8,8> generate8x8();


int main() {
    test_solve_3();
    cout << "\n\n\n\nTesting 8x8:" << endl;
    test_solve_8();

    return 0;
}

void test_solve_3() {
    Matrix<3, 3> A(true);
//array([[ 2,  3, -3],
//       [-4,  2,  3],
//       [-3, -3,  4]])
    A.set(0, 0, 2);
    A.set(0, 1, 3);
    A.set(0, 2, -3);

    A.set(1, 0, -4);
    A.set(1, 1, 2);
    A.set(1, 2, 3);

    A.set(2, 0, -3);
    A.set(2, 1, -3);
    A.set(2, 2, 4);

    LU<3> lu;
    lu.factorize(A);
    auto& L = lu.getL();
    auto& U = lu.getU();

    Matrix<3, 1> b; // [1, 2, 3]
    b.set(0, 0, 1);
    b.set(1, 0, 2);
    b.set(2, 0, 3);

    Matrix<3, 1> btest;
    Matrix<3, 1> x;

    lu.solve(b, x);
    A.multiply(x, btest);

    cout << "A:\n" << A << endl;
    cout << "b:\n"<< b << endl;

    cout << "x (Ax = b):\n"<< x << endl;
    cout << "Ax (should be b again):\n"<< btest << endl;

    Matrix<3, 3> Ainv;
    Matrix<3, 3> AAinv;

    A.inv(Ainv);
    A.multiply(Ainv, AAinv);

    cout << "A^-1:\n"<< Ainv << endl;
    cout << "A * A^-1 (should be I):\n"<< AAinv << endl;
}

void test_solve_8() {
    auto A = generate8x8();
    LU<8> lu;
    assert(lu.factorize(A));

    Matrix<8, 1> b(true);
    Matrix<8, 1> btest(true);
    Matrix<8, 1> x;

    b.set(0, 0, 1);
    b.set(2, 0, 2);
    b.set(4, 0, 3);
    b.set(6, 0, 4);

    lu.solve(b, x);
    A.multiply(x, btest);
    btest.clamp(1e-12);

    cout << "A:\n" << A << endl;
    cout << "b:\n"<< b << endl;

    cout << "x (Ax = b):\n"<< x << endl;
    cout << "Ax (should be b again):\n"<< btest << endl;

    Matrix<8, 8> Ainv;
    Matrix<8, 8> AAinv;

    A.inv(Ainv);
    A.multiply(Ainv, AAinv);
    AAinv.clamp(1e-12);

    cout << "A^-1:\n"<< Ainv << endl;
    cout << "A * A^-1 (should be I):\n"<< AAinv << endl;
}

/**
 * array([[ 3.0,  4, -1, -2,  1,  2, -1,  0],
       [ 2,  1, -4, -2, -4,  3,  1,  2],
       [ 1,  3, -3,  0, -1,  3, -1, -3],
       [ 3, -3,  2,  3,  2,  1, -3,  1],
       [-3,  1,  3,  0, -1, -3, -1,  1],
       [-1,  0,  4,  1,  4, -1, -1, -2],
       [-1, -4,  0,  1,  2,  3, -1, -1],
       [-1, -3, -1,  2, -1, -4,  2,  1]])
 */
Matrix<8,8> generate8x8() {
    Matrix<8, 8> A;
    A.set(0, 0, 3);
    A.set(0, 1, 4);
    A.set(0, 2, -1);
    A.set(0, 3, -2);
    A.set(0, 4, 1);
    A.set(0, 5, 2);
    A.set(0, 6, -1);
    A.set(0, 7, 0);
    A.set(1, 0, 2);
    A.set(1, 1, 1);
    A.set(1, 2, -4);
    A.set(1, 3, -2);
    A.set(1, 4, -4);
    A.set(1, 5, 3);
    A.set(1, 6, 1);
    A.set(1, 7, 2);
    A.set(2, 0, 1);
    A.set(2, 1, 3);
    A.set(2, 2, -3);
    A.set(2, 3, 0);
    A.set(2, 4, -1);
    A.set(2, 5, 3);
    A.set(2, 6, -1);
    A.set(2, 7, -3);
    A.set(3, 0, 3);
    A.set(3, 1, -3);
    A.set(3, 2, 2);
    A.set(3, 3, 3);
    A.set(3, 4, 2);
    A.set(3, 5, 1);
    A.set(3, 6, -3);
    A.set(3, 7, 1);
    A.set(4, 0, -3);
    A.set(4, 1, 1);
    A.set(4, 2, 3);
    A.set(4, 3, 0);
    A.set(4, 4, -1);
    A.set(4, 5, -3);
    A.set(4, 6, -1);
    A.set(4, 7, 1);
    A.set(5, 0, -1);
    A.set(5, 1, 0);
    A.set(5, 2, 4);
    A.set(5, 3, 1);
    A.set(5, 4, 4);
    A.set(5, 5, -1);
    A.set(5, 6, -1);
    A.set(5, 7, -2);
    A.set(6, 0, -1);
    A.set(6, 1, -4);
    A.set(6, 2, 0);
    A.set(6, 3, 1);
    A.set(6, 4, 2);
    A.set(6, 5, 3);
    A.set(6, 6, -1);
    A.set(6, 7, -1);
    A.set(7, 0, -1);
    A.set(7, 1, -3);
    A.set(7, 2, -1);
    A.set(7, 3, 2);
    A.set(7, 4, -1);
    A.set(7, 5, -4);
    A.set(7, 6, 2);
    A.set(7, 7, 1);
    return A;
}