//
// Created by Anandh Swaminathan on 10/16/22.
//
#include <iostream>

#ifndef MATRIX_MATRIX_H
#define MATRIX_MATRIX_H
template<unsigned int M, unsigned int N>
class Matrix {
private:
    const unsigned size = M * N;
    double data[M * N];

public:
    Matrix(bool zero = false);

    Matrix(const Matrix<M, N>& from);

    void set(int row, int col, double value);

    double get(int row, int col) const;

    template <unsigned int P>
    void multiply(const Matrix<N, P> &that, Matrix<M, P> &dest) const;

    void fill(double value);

    void zeroOut();

    void setIdentity();

    void add(const Matrix<M, N> &that, Matrix<M, N> &dest);

    void add(const double that, Matrix<M, N> &dest);

    bool inv(Matrix<M, N> &dest);

    void clamp(double tolerance);
};

template<unsigned int M, unsigned int N>
std::ostream &operator<<(std::ostream &os, Matrix<M, N> const &m);

#include "matrix.cpp"

#endif //MATRIX_MATRIX_H
